<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function browsing_history_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
}

/**
 * Implementation of hook_flag_default_flags().
 */
function browsing_history_flag_default_flags() {
  $flags = array();
  // Exported flag: "Browsing History Categories".
  $flags['browsing_history_categories'] = array(
    'content_type' => 'term',
    'title' => 'Browsing History Categories',
    'global' => '0',
    'types' => array(
      '0' => '1',
    ),
    'flag_short' => 'Add category to browsing history',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        '0' => 1,
        '1' => 2,
      ),
      'unflag' => array(
        '0' => 1,
        '1' => 2,
      ),
    ),
    'show_on_term_page' => 0,
    'module' => 'browsing_history',
    'locked' => array(
      '0' => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Browsing History Content".
  $flags['browsing_history_content'] = array(
    'content_type' => 'node',
    'title' => 'Browsing History Content',
    'global' => '0',
    'types' => array(
      '0' => 'story',
    ),
    'flag_short' => 'Add to browsing history',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        '0' => 1,
        '1' => 2,
      ),
      'unflag' => array(
        '0' => 1,
        '1' => 2,
      ),
    ),
    'show_on_page' => 0,
    'show_on_teaser' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'browsing_history',
    'locked' => array(
      '0' => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Browsing History Search".
  $flags['browsing_history_search'] = array(
    'content_type' => 'page',
    'title' => 'Browsing History Search',
    'global' => '0',
    'types' => array(),
    'flag_short' => 'Add to your browsing history',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => '[x]',
    'unflag_long' => 'Remove from your browsing history',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        '0' => 1,
        '1' => 2,
      ),
      'unflag' => array(
        '0' => 1,
        '1' => 2,
      ),
    ),
    'override_page_title' => 0,
    'module' => 'browsing_history',
    'locked' => array(
      '0' => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Browsing history switch".
  $flags['browsing_history_switch'] = array(
    'content_type' => 'flag_state',
    'title' => 'Browsing history switch',
    'global' => '0',
    'types' => array(),
    'flag_short' => 'Turn OFF browsing history',
    'flag_long' => 'Turn browsing history off.',
    'flag_message' => 'Browsing history has been turned off.',
    'unflag_short' => 'Turn ON browsing history',
    'unflag_long' => 'Turn browsing history on.',
    'unflag_message' => 'Browsing history has been turned on.',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'roles' => array(
      'flag' => array(
        '0' => 1,
        '1' => 2,
      ),
      'unflag' => array(
        '0' => 1,
        '1' => 2,
      ),
    ),
    'flag_confirmation_desc' => 'When you turn OFF your browsing history, your browsing history will be cleared.',
    'unflag_confirmation_desc' => '',
    'flag_confirmation' => 'Do you want to turn OFF and CLEAR your browsing history?',
    'unflag_confirmation' => 'Do you want to turn ON browsing history?',
    'module' => 'browsing_history',
    'locked' => array(
      '0' => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implementation of hook_rules_defaults().
 */
function browsing_history_rules_defaults() {
  return array(
    'rules' => array(
      'rules_add_search_to_browsing_history' => array(
        '#type' => 'rule',
        '#set' => 'event_search_page_view',
        '#label' => 'Add search to browsing history',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          'browsing_history' => 'browsing_history',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#weight' => 0,
            '#negate' => 1,
            '#info' => array(
              'label' => 'Browsing history disabled',
              'label callback' => FALSE,
              'module' => 'Flag State',
              'eval input' => array(
                '0' => 'flag_name',
              ),
            ),
            '#name' => 'flag_state_flag_condition',
            '#type' => 'condition',
            '#settings' => array(
              'flag_name' => 'browsing_history_switch',
            ),
          ),
        ),
        '#actions' => array(
          '5' => array(
            '#type' => 'action',
            '#settings' => array(
              'flag' => 'browsing_history_search',
              'permission_check' => 0,
              '#argument map' => array(
                'object' => 'search_path',
                'flagging_user' => 'user',
              ),
            ),
            '#name' => 'flag_rules_action_unflag_page',
            '#info' => array(
              'label' => 'Unflag Search path, under "Browsing History Search"',
              'base' => 'flag_rules_action_unflag',
              'label callback' => 'flag_rules_action_unflag_label',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                  'flag_type' => 'page',
                ),
                'object' => array(
                  'type' => 'page',
                  'label' => 'Flagged page',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User on whose behalf to unflag',
                  'description' => 'For non-global flags, this is the user on whose behalf to unflag the object. In addition, if checked below, the access permissions to the flag are checked against this user.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#weight' => -1,
          ),
          '4' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Flag Search path, under "Browsing History Search"',
              'base' => 'flag_rules_action_flag',
              'label callback' => 'flag_rules_action_flag_label',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                  'flag_type' => 'page',
                ),
                'object' => array(
                  'type' => 'page',
                  'label' => 'Flagged page',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User on whose behalf to flag',
                  'description' => 'For non-global flags, this is the user on whose behalf to flag the object. In addition, if checked below, the access permissions to the flag are checked against this user.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#name' => 'flag_rules_action_flag_page',
            '#settings' => array(
              'flag' => 'browsing_history_search',
              'permission_check' => 0,
              '#argument map' => array(
                'object' => 'search_path',
                'flagging_user' => 'user',
              ),
            ),
            '#type' => 'action',
          ),
          '1' => array(
            '#weight' => 1,
            '#info' => array(
              'label' => 'Trim "Browsing History Search" at 10',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User whose flag to trim',
                  'description' => 'For non-global flags, this is the user whose flag to trim. (For global flags, this argument is ignored.)',
                ),
                'cutoff_size' => array(
                  'type' => 'number',
                  'label' => 'Flag queue size',
                  'description' => 'The maximum number of objects to keep in the queue. Newly flagged objects will be kept; older ones will be removed. Tip: by typing "1" here you implement a singleton.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#name' => 'flag_rules_action_trim',
            '#settings' => array(
              'flag' => 'browsing_history_search',
              'cutoff_size' => '10',
              '#argument map' => array(
                'flagging_user' => 'user',
              ),
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
      ),
      'rules_add_viewed_category_to_browsing_history' => array(
        '#type' => 'rule',
        '#set' => 'event_term_page_view',
        '#label' => 'Add viewed category to browsing history',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          'browsing_history' => 'browsing_history',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#info' => array(
              'label' => 'Browsing history disabled',
              'label callback' => FALSE,
              'module' => 'Flag State',
              'eval input' => array(
                '0' => 'flag_name',
              ),
            ),
            '#negate' => 1,
            '#name' => 'flag_state_flag_condition',
            '#settings' => array(
              'flag_name' => 'browsing_history_switch',
            ),
            '#weight' => 0,
            '#type' => 'condition',
          ),
          '1' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Viewed term in rebi',
              'arguments' => array(
                'term' => array(
                  'type' => 'taxonomy_term',
                  'label' => 'Term',
                ),
              ),
              'module' => 'Taxonomy',
            ),
            '#name' => 'rules_condition_taxonomy_term_in_vocabulary',
            '#settings' => array(
              'vocab' => array(
                '1' => '1',
              ),
              '#argument map' => array(
                'term' => 'term',
              ),
            ),
            '#type' => 'condition',
          ),
        ),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#type' => 'action',
            '#settings' => array(
              'flag' => 'browsing_history_categories',
              'permission_check' => 0,
              '#argument map' => array(
                'object' => 'term',
                'flagging_user' => 'user',
              ),
            ),
            '#name' => 'flag_rules_action_unflag_term',
            '#info' => array(
              'label' => 'Unflag viewed term, under "Browsing History Categories"',
              'base' => 'flag_rules_action_unflag',
              'label callback' => 'flag_rules_action_unflag_label',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                  'flag_type' => 'term',
                ),
                'object' => array(
                  'type' => 'taxonomy_term',
                  'label' => 'Flagged term',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User on whose behalf to unflag',
                  'description' => 'For non-global flags, this is the user on whose behalf to unflag the object. In addition, if checked below, the access permissions to the flag are checked against this user.',
                ),
              ),
              'module' => 'Flag',
            ),
          ),
          '1' => array(
            '#info' => array(
              'label' => 'Flag viewed term, under "Browsing History Categories"',
              'base' => 'flag_rules_action_flag',
              'label callback' => 'flag_rules_action_flag_label',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                  'flag_type' => 'term',
                ),
                'object' => array(
                  'type' => 'taxonomy_term',
                  'label' => 'Flagged term',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User on whose behalf to flag',
                  'description' => 'For non-global flags, this is the user on whose behalf to flag the object. In addition, if checked below, the access permissions to the flag are checked against this user.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#name' => 'flag_rules_action_flag_term',
            '#settings' => array(
              'flag' => 'browsing_history_categories',
              'permission_check' => 0,
              '#argument map' => array(
                'object' => 'term',
                'flagging_user' => 'user',
              ),
            ),
            '#type' => 'action',
            '#weight' => 0,
          ),
          '2' => array(
            '#type' => 'action',
            '#settings' => array(
              'flag' => 'browsing_history_categories',
              'cutoff_size' => '20',
              '#argument map' => array(
                'flagging_user' => 'user',
              ),
            ),
            '#name' => 'flag_rules_action_trim',
            '#info' => array(
              'label' => 'Trim "Browsing History Categories" at 20',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User whose flag to trim',
                  'description' => 'For non-global flags, this is the user whose flag to trim. (For global flags, this argument is ignored.)',
                ),
                'cutoff_size' => array(
                  'type' => 'number',
                  'label' => 'Flag queue size',
                  'description' => 'The maximum number of objects to keep in the queue. Newly flagged objects will be kept; older ones will be removed. Tip: by typing "1" here you implement a singleton.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#weight' => 1,
          ),
        ),
        '#version' => 6003,
      ),
      'rules_add_viewed_content_to_browsing_history' => array(
        '#type' => 'rule',
        '#set' => 'event_node_page_view',
        '#label' => 'Add viewed content to browsing history',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          'browsing_history' => 'browsing_history',
        ),
        '#status' => 'default',
        '#conditions' => array(
          '0' => array(
            '#info' => array(
              'label' => 'Browsing history disabled',
              'label callback' => FALSE,
              'module' => 'Flag State',
              'eval input' => array(
                '0' => 'flag_name',
              ),
            ),
            '#negate' => 1,
            '#name' => 'flag_state_flag_condition',
            '#settings' => array(
              'flag_name' => 'browsing_history_switch',
            ),
            '#weight' => 0,
            '#type' => 'condition',
          ),
          '1' => array(
            '#type' => 'condition',
            '#settings' => array(
              'type' => array(
                'story' => 'story',
              ),
              '#argument map' => array(
                'node' => 'node',
              ),
            ),
            '#name' => 'rules_condition_content_is_type',
            '#info' => array(
              'label' => 'Viewed content is Story',
              'arguments' => array(
                'node' => array(
                  'type' => 'node',
                  'label' => 'Content',
                ),
              ),
              'module' => 'Node',
            ),
            '#weight' => 0,
          ),
        ),
        '#actions' => array(
          '2' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Unflag viewed content, under "Browsing History Content"',
              'base' => 'flag_rules_action_unflag',
              'label callback' => 'flag_rules_action_unflag_label',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                  'flag_type' => 'node',
                ),
                'object' => array(
                  'type' => 'node',
                  'label' => 'Flagged content',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User on whose behalf to unflag',
                  'description' => 'For non-global flags, this is the user on whose behalf to unflag the object. In addition, if checked below, the access permissions to the flag are checked against this user.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#name' => 'flag_rules_action_unflag_node',
            '#settings' => array(
              'flag' => 'browsing_history_content',
              'permission_check' => 0,
              '#argument map' => array(
                'object' => 'node',
                'flagging_user' => 'user',
              ),
            ),
            '#type' => 'action',
          ),
          '3' => array(
            '#type' => 'action',
            '#settings' => array(
              'flag' => 'browsing_history_content',
              'permission_check' => 0,
              '#argument map' => array(
                'object' => 'node',
                'flagging_user' => 'user',
              ),
            ),
            '#name' => 'flag_rules_action_flag_node',
            '#info' => array(
              'label' => 'Flag viewed content, under "Browsing History Content"',
              'base' => 'flag_rules_action_flag',
              'label callback' => 'flag_rules_action_flag_label',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                  'flag_type' => 'node',
                ),
                'object' => array(
                  'type' => 'node',
                  'label' => 'Flagged content',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User on whose behalf to flag',
                  'description' => 'For non-global flags, this is the user on whose behalf to flag the object. In addition, if checked below, the access permissions to the flag are checked against this user.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#weight' => 0,
          ),
          '1' => array(
            '#weight' => 1,
            '#type' => 'action',
            '#settings' => array(
              'flag' => 'browsing_history_content',
              'cutoff_size' => '40',
              '#argument map' => array(
                'flagging_user' => 'user',
              ),
            ),
            '#name' => 'flag_rules_action_trim',
            '#info' => array(
              'label' => 'Trim "Browsing History Content" at 40',
              'label callback' => FALSE,
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User whose flag to trim',
                  'description' => 'For non-global flags, this is the user whose flag to trim. (For global flags, this argument is ignored.)',
                ),
                'cutoff_size' => array(
                  'type' => 'number',
                  'label' => 'Flag queue size',
                  'description' => 'The maximum number of objects to keep in the queue. Newly flagged objects will be kept; older ones will be removed. Tip: by typing "1" here you implement a singleton.',
                ),
              ),
              'module' => 'Flag',
            ),
          ),
        ),
        '#version' => 6003,
      ),
      'rules_clear_browsing_history' => array(
        '#type' => 'rule',
        '#set' => 'event_flag_flagged_browsing_history_switch',
        '#label' => 'Clear browsing history',
        '#active' => 1,
        '#weight' => '0',
        '#categories' => array(
          'browsing_history' => 'browsing_history',
        ),
        '#status' => 'default',
        '#conditions' => array(),
        '#actions' => array(
          '0' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Trim "Browsing History Content" at 0',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User whose flag to trim',
                  'description' => 'For non-global flags, this is the user whose flag to trim. (For global flags, this argument is ignored.)',
                ),
                'cutoff_size' => array(
                  'type' => 'number',
                  'label' => 'Flag queue size',
                  'description' => 'The maximum number of objects to keep in the queue. Newly flagged objects will be kept; older ones will be removed. Tip: by typing "1" here you implement a singleton.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#name' => 'flag_rules_action_trim',
            '#settings' => array(
              'flag' => 'browsing_history_content',
              'cutoff_size' => '0',
              '#argument map' => array(
                'flagging_user' => 'flagging_user',
              ),
            ),
            '#type' => 'action',
          ),
          '1' => array(
            '#type' => 'action',
            '#settings' => array(
              'flag' => 'browsing_history_categories',
              'cutoff_size' => '0',
              '#argument map' => array(
                'flagging_user' => 'flagging_user',
              ),
            ),
            '#name' => 'flag_rules_action_trim',
            '#info' => array(
              'label' => 'Trim "Browsing History Categories" at 0',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User whose flag to trim',
                  'description' => 'For non-global flags, this is the user whose flag to trim. (For global flags, this argument is ignored.)',
                ),
                'cutoff_size' => array(
                  'type' => 'number',
                  'label' => 'Flag queue size',
                  'description' => 'The maximum number of objects to keep in the queue. Newly flagged objects will be kept; older ones will be removed. Tip: by typing "1" here you implement a singleton.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#weight' => 0,
          ),
          '2' => array(
            '#weight' => 0,
            '#info' => array(
              'label' => 'Trim "Browsing History Search" at 0',
              'arguments' => array(
                'flag' => array(
                  'type' => 'flag',
                  'label' => 'Flag',
                ),
                'flagging_user' => array(
                  'type' => 'user',
                  'label' => 'User whose flag to trim',
                  'description' => 'For non-global flags, this is the user whose flag to trim. (For global flags, this argument is ignored.)',
                ),
                'cutoff_size' => array(
                  'type' => 'number',
                  'label' => 'Flag queue size',
                  'description' => 'The maximum number of objects to keep in the queue. Newly flagged objects will be kept; older ones will be removed. Tip: by typing "1" here you implement a singleton.',
                ),
              ),
              'module' => 'Flag',
            ),
            '#name' => 'flag_rules_action_trim',
            '#settings' => array(
              'flag' => 'browsing_history_search',
              'cutoff_size' => '0',
              '#argument map' => array(
                'flagging_user' => 'flagging_user',
              ),
            ),
            '#type' => 'action',
          ),
        ),
        '#version' => 6003,
      ),
    ),
  );
}

/**
 * Implementation of hook_views_api().
 */
function browsing_history_views_api() {
  return array(
    'api' => '2',
  );
}
