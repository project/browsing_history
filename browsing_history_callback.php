<?php
/**
 * Authcache callback function for browsing history. Fire item viewed trigger (or implementation of) and return blocks
 * that need updating.
 *
 * @todo Check this "Including in same method ensures trigger is fired first before block is generated".
 *
 * @todo Improve performance by cutting out full bootstrap (flags, views etc) and going straight to the database. Needs
 * to be fairly sophisticated to handle anonymous users and ensure security etc.
 *
 * @param $vars
 *  This is the value that was included in the authcache ajax object of the cached page by as_browsing_history_authcache_ajax().
 *
 * @return
 *  JSON containing the user history block we wish to update.
 */
// function browsing_history_content_viewed($vars) {
function _authcache_browsing_history_content($vars) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  $nid = $vars;
  if($node = node_load($nid)) {
    if(module_exists('rules')) {
      // $arguments = array('node' => &$node, 'teaser' => FALSE, 'page' => TRUE);
      // rules_invoke_event('node_view', $arguments);
      rules_invoke_event('context_active_browsing_history_content');
    }
  }
}

function _authcache_browsing_history_category($vars) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  $tid = $vars;
  if($term = taxonomy_get_term($tid)) {
    if(module_exists('rules')) {
      $arguments = array();
      rules_invoke_event('init', $arguments);
    }
  }
}

function _authcache_browsing_history_search($vars) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  if(module_exists('rules')) {
    rules_invoke_event('init', array());
  }
}

/**
 * Return the users browsing history view block.
 */
// function browsing_history_content_view($vars) {
function _authcache_browsing_history_view($vars) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  $block = 'views-browsing_history_nodes-block_1';
  $parts = explode("-",$block,2);

  $blocks_content = module_invoke($parts[0], 'block', 'view', $parts[1]);
  $content[$block] = $blocks_content['content'];

  return array('msg' => 'OK', 'content' => $content);
}

?>