<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function browsing_history_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsing_history_content';
  $context->description = 'Context to add browsing history block to';
  $context->tag = 'browsing history';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'story' => 'story',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-browsing_history_nodes-block_1' => array(
          'module' => 'views',
          'delta' => 'browsing_history_nodes-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context to add browsing history block to');
  t('browsing history');

  $export['browsing_history_content'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsing_history_user_page';
  $context->description = '';
  $context->tag = 'browsing history';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'browsing_history_nodes:page' => 'browsing_history_nodes:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-browsing_history_terms-block_1' => array(
          'module' => 'views',
          'delta' => 'browsing_history_terms-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
        'views-browsing_history_search-block_1' => array(
          'module' => 'views',
          'delta' => 'browsing_history_search-block_1',
          'region' => 'right',
          'weight' => 1,
        ),
      ),
    ),
    'breadcrumb' => 'history',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('browsing history');

  $export['browsing_history_user_page'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsing_history_user_page_categories';
  $context->description = '';
  $context->tag = 'browsing history';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'browsing_history_terms:page_1' => 'browsing_history_terms:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-browsing_history_nodes-block_1' => array(
          'module' => 'views',
          'delta' => 'browsing_history_nodes-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
        'views-browsing_history_search-block_1' => array(
          'module' => 'views',
          'delta' => 'browsing_history_search-block_1',
          'region' => 'right',
          'weight' => 1,
        ),
      ),
    ),
    'breadcrumb' => 'history',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('browsing history');

  $export['browsing_history_user_page_categories'] = $context;
  return $export;
}
